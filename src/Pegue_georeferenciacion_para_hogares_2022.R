# install.packages("tidyverse")
# install.packages("pacman")
# install.packages("leaflet")
# install.packages("ggmap")
rm(list=ls())
library(pacman)
p_load(tidyverse,openxlsx,sf, haven, broom, leaflet, ggmap)
options(digits = 8, scipen = 99)

# base_hogares=read_sas("input/hogares_elco2022.sas7bdat") 
base_hogares <- read.xlsx("input/hogares_elco2022.xlsx")
base_hogares<- base_hogares %>% #rename(c("LATITUD"="P810S1", "LONGITUD"="P810S2")) %>% 
                 mutate(extraction = str_extract(P1663, "^\\d+\\.\\d+\\s-\\d+\\.\\d+"))
head(base_hogares)

# base_hogares_v2 <- base_hogares %>%
#   mutate(extraction = str_extract(P1663, "^\\d+\\.\\d+\\s-\\d+\\.\\d+"))

split_matrix <- do.call(rbind, str_split(base_hogares$extraction, "\\s"))
extraction_df <- data.frame(split_matrix) %>% rename(c("LATITUD_EXTRACT"="X1", "LONGITUD_EXTRACT"="X2"))

base_hogares_v2 <- base_hogares %>% cbind(extraction_df) %>% 
  select (DIRECTORIO, LATITUD, LONGITUD, LATITUD_EXTRACT, LONGITUD_EXTRACT) %>% distinct()

to_numeric <- function(x){
  round(as.numeric(as.character(x)), 5)
}

base_hogares_v3 <- base_hogares_v2 %>% 
  mutate_at(c("LATITUD", "LONGITUD", "LATITUD_EXTRACT", "LONGITUD_EXTRACT"), to_numeric)

base_hogares_v4  <- base_hogares_v3 %>% 
  mutate(LATITUD = ifelse(is.na(LATITUD), LATITUD_EXTRACT, LATITUD),
         LONGITUD = ifelse(is.na(LONGITUD), LONGITUD_EXTRACT, LONGITUD))

base_hogares_v5 <- base_hogares_v4 %>% filter(!is.na(LATITUD)) 
directorio_sin_georef <-base_hogares_v4 %>% filter(is.na(LATITUD)) %>% select(DIRECTORIO)
# list.files()

Urbano_manz <- st_read("input/MGN/MGN_URB_MANZANA.shp"); head(Urbano_manz, 4)
Rural_secc <- st_read("input/MGN/MGN_RUR_SECCION.shp") ; head(Rural_secc, 4)

Urbano_manzanas <- Urbano_manz %>% 
  dplyr::select(DPTO_CCDGO, MPIO_CCDGO, SECR_CCDGO, MANZ_CCDGO, MANZ_CCNCT) 

Rural_seccion <- Rural_secc %>%  
  dplyr::select(DPTO_CCDGO, MPIO_CCDGO, CLAS_CCDGO, SECR_CCNCT, SETR_CCDGO, SECR_CCDGO)

# SPDF <- st_transform( Urbano_manzanas,  CRS("+ellps=WGS84 +proj=longlat +datum=WGS84 +no_defs"))
# map <- get_map(location = c(-74.2, 11.2, 74, 11),maptype = "roadmap", source = "osm", zoom = 11)   
# ggmap( ) %>% addPolygons(data=SPDF)
# plot(st_geometry(Urbano_manzanas_2) )


#Asignamos el sistema  de coordenadas de referencia 
coordenadas <- st_as_sf(base_hogares_v5, coords=c("LONGITUD", "LATITUD"), crs=4326)

# emparejamos sistemas de coordenadas de referencia
st_crs(Urbano_manzanas) <- st_crs(coordenadas)
sf_use_s2(FALSE)
Urbano_manzanas_2 <- st_join(coordenadas, Urbano_manzanas, join=st_intersects)
Urbano_manzanas_2 %>% class

Urbano_manzanas_2 %>% as.data.frame() %>% select(MANZ_CCNCT) %>% 
  mutate(LONGITUD = nchar(MANZ_CCNCT)) %>% group_by(LONGITUD) %>% count()

Urbano_manzanas_2 <- cbind(Urbano_manzanas_2, st_coordinates(Urbano_manzanas_2$geometry)) %>% 
  select(-c(LATITUD_EXTRACT ,LONGITUD_EXTRACT )) %>% 
  rename(c("LATITUD_EXTRACT"="X", "LONGITUD_EXTRACT"="Y"))
Urbano_manzanas_2 %>% head()

##

st_crs(Rural_seccion) <- st_crs(coordenadas)
sf_use_s2(FALSE)
Rural_seccion_2 <- st_join(coordenadas, Rural_seccion, join=st_intersects)

Rural_seccion_2 %>% as.data.frame() %>% select(SECR_CCNCT) %>% 
  mutate(LONGITUD = nchar(SECR_CCNCT)) %>% group_by(LONGITUD) %>% count()

Rural_seccion_2 <- cbind(Rural_seccion_2, st_coordinates(Rural_seccion_2$geometry)) %>% 
  select(-c(LATITUD_EXTRACT ,LONGITUD_EXTRACT )) %>% 
  rename(c("LATITUD_EXTRACT"="X", "LONGITUD_EXTRACT"="Y"))
head(Rural_seccion_2)

# registros no vacios 9.377 de URBANO(manz) y 15.409 de RURAL(secc) 
Urbano_manzanas_3 <- Urbano_manzanas_2 %>% filter(!is.na(Urbano_manzanas_2$MANZ_CCNCT)) 
Rural_seccion_3 <- Rural_seccion_2 %>% filter(!is.na(Rural_seccion_2$SECR_CCNCT)) 

Urbano <- st_drop_geometry(Urbano_manzanas_3) %>% as.data.frame() %>% 
       select(DIRECTORIO, MANZ_CCDGO , MANZ_CCNCT)
Rural <-  st_drop_geometry(Rural_seccion_3) %>% as.data.frame()   

# names(Urbano); names(Rural)

#Pegue de resultados
Urbano_rural_1 <- Urbano %>% inner_join(Rural, by = c("DIRECTORIO" = "DIRECTORIO")) %>%
          select(-c(SETR_CCDGO, SECR_CCDGO))
Urbano_rural_1 %>% head()
Urbano_rural_2 <- Rural %>% filter (!(DIRECTORIO %in% unique(Urbano_rural_1$DIRECTORIO)))


Urbano_rural_2_V2 <- Urbano_rural_2 %>% 
  mutate(MANZ_CCDGO="", MANZ_CCNCT= paste0(DPTO_CCDGO, MPIO_CCDGO, CLAS_CCDGO, SETR_CCDGO, SECR_CCDGO, "000", "0000", "00", "00") ) %>%
  select(DIRECTORIO, MANZ_CCDGO, MANZ_CCNCT, LATITUD_EXTRACT, LONGITUD_EXTRACT, DPTO_CCDGO, MPIO_CCDGO, CLAS_CCDGO, SECR_CCNCT )

Urbano_rural_3 <- rbind(Urbano_rural_1, Urbano_rural_2_V2) %>% as.data.frame()

Urbano_rural_3 %>% mutate(len = nchar(MANZ_CCNCT)) %>% group_by(len) %>% count()

faltantes <- Urbano_manzanas_2 %>% filter (!(DIRECTORIO %in% unique(Urbano_rural_3$DIRECTORIO))) %>% 
  mutate(CLAS_CCDGO="", SECR_CCNCT="") %>% 
  select(DIRECTORIO, MANZ_CCDGO, MANZ_CCNCT, LATITUD_EXTRACT, LONGITUD_EXTRACT, DPTO_CCDGO, MPIO_CCDGO, CLAS_CCDGO, SECR_CCNCT )

faltantes2 <- st_drop_geometry(faltantes) %>% as.data.frame()   

## Base con 15411 registros (directorio) con el dato de LONGITUD- LATITUD 
##  de lso cuales 15.409  se asignó MANZ_CCNCT

Urbano_rural_4 <- Urbano_rural_3 %>% rbind(faltantes2)

# write.xlsx(Urbano_rural_4, "input/hogares_elco2022_georef.xlsx")


#########  REVISION ADICIONAL  ##############

LISTA_MPIOS_HOG <- base_hogares %>% dplyr::select(DIRECTORIO, MPIO) %>% distinct()
LISTA_MPIOS_GEOREF <- Urbano_rural_4 %>%  mutate(MPIO_GEO = substr(MANZ_CCNCT, 1, 5)) %>% 
  dplyr::select(DIRECTORIO, MPIO_GEO) %>% distinct() 

### 253 MPIOS QUE NO COINCIDEN EN LA BASE HOGARES  VERSUS BASE GEOREGERENCIACION
LISTA_MPIOS_HOG %>% left_join(LISTA_MPIOS_GEOREF) %>% select(MPIO, MPIO_GEO) %>% 
  distinct() %>% filter(MPIO!=MPIO_GEO)
