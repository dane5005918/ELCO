source("src/Functions_to_make_qcc.R")

# Procesamiento sobre el shape de Municipios ------------------------------

mun_shp <- st_read("input/shapes/shapes.shp")
mun_shp_sp_object <- as(mun_shp, "Spatial")

# Convert sp object to SpatialPolygonsDataFrame
mun_shp_spdf <- as(mun_shp_sp_object, "SpatialPolygonsDataFrame")

# Convert SpatialPolygonsDataFrame to sf object
sf_object <- st_as_sf(mun_shp_spdf)
# Calculate centroids
centroids <- st_centroid(sf_object)

centroides <- st_coordinates(centroids)
df_centroides <- data.frame(DIVIPOLA = paste0(sf_object$dpto, sf_object$mpio)) %>%
  cbind(centroides) %>%
  rename("Longitud_centroid" = "X", "Latitud_centroid" = "Y")

coordinates(df_centroides) <- ~ Longitud_centroid + Latitud_centroid
proj4string(df_centroides) <- CRS("+init=epsg:4326")
xy = data.frame(spTransform(df_centroides, CRS("+init=epsg:2804")))
df_coord_plane_mpio <- xy %>% dplyr::select(-"optional") %>% 
  rename("Longitud" = "coords.x1", "Latitud" = "coords.x2")

# df_centroides <- df_centroides %>% filter(!duplicated(DIVIPOLA))
df_coord_plane_mpio <- df_coord_plane_mpio %>% filter(!duplicated(DIVIPOLA))

# Procesamiento sobre los shapes de Departamentos  ------------------------

# Lectura del shape a nivel de municipios
dpto_shp <- st_read("input/Departamentos/Departamentos.shp")
dpto_shp_sp_object <- as(dpto_shp, "Spatial")

# Convert sp object to SpatialPolygonsDataFrame
dpto_shp_spdf <- as(dpto_shp_sp_object, "SpatialPolygonsDataFrame")

# Convert SpatialPolygonsDataFrame to sf object
sf_object <- st_as_sf(dpto_shp_spdf)
# Calculate centroids

centroids <- st_centroid(sf_object)

centroides <- st_coordinates(centroids)
df_centroides <- data.frame(CODIGO_DEPARTAMENTO = paste0(sf_object$DPTO_CCDGO)) %>%
  cbind(centroides) %>%
  rename("Longitud_centroid" = "X", "Latitud_centroid" = "Y")

coordinates(df_centroides) <- ~ Longitud_centroid + Latitud_centroid
proj4string(df_centroides) <- CRS("+init=epsg:4326")
xy = data.frame(spTransform(df_centroides, CRS("+init=epsg:2804")))
df_coord_plane_dpto <- xy %>% dplyr::select(-"optional") %>% 
  rename("Longitud" = "coords.x1", "Latitud" = "coords.x2")
df_coord_plane_dpto <- df_coord_plane_dpto %>% filter(!duplicated(CODIGO_DEPARTAMENTO))


# Procesamiento sobre la base de datos de avalúos -----------------------------------

optional_files = list.files("../CONSOLIDACION_INFORMACION_OIC/output/", pattern = "BASE_FINAL_CONSOLIDADA_1_1.parquet")
print(sort(optional_files, decreasing = TRUE)[1])
base_consolidada = read_parquet(paste0("../CONSOLIDACION_INFORMACION_OIC/output/", sort(optional_files, decreasing = TRUE)[1]))

df_r1 <- read_parquet("../BASES_R1_R2_PROCESADAS/2401_R1_UNIQUE.parquet")
list_divipola_r1 <- unique(substr(df_r1$NUMERO_PREDIAL_COMPLETO, 1, 5))
rm(df_r1)
# Cambiar esta parte para el marco geográfico
base_consolidada_RUR <- base_consolidada %>% 
  filter(substr(CODIGO_PREDIAL, 6, 7) == "00") %>% 
  filter(FUENTE_INFO != "ALCALDIAS") %>%  #(YEAR >= 2018 | is.na(YEAR)) & 
  filter(TIENE_VR_TERRENO == "SI") %>% 
  filter(DIVIPOLA %in% list_divipola_r1) %>% 
  mutate(VALOR_TERRENO_COM_HECTAREA = ifelse(!is.na(VALOR_TERRENO_COM_HECTAREA),
         VALOR_TERRENO_COM_HECTAREA, (Avaluo_Terreno/Area_Terreno_FIN)*10000)) %>% 
  filter(!is.na(VALOR_TERRENO_COM_HECTAREA)) %>% 
  filter(!duplicated(paste0(CODIGO_PREDIAL, round(VALOR_TERRENO_COM_HECTAREA)))) %>% 
  mutate(LOG_VALOR_TERRENO_COM_HECTAREA = log(VALOR_TERRENO_COM_HECTAREA),
         LOG_Area_Terreno_FIN = log(Area_Terreno_FIN)) %>% 
mutate(FUENTE_INFO_V2 = ifelse(FUENTE_INFO == "SNR", "TRANSACCION", "AVALUOS_OFERTAS"),
       CODIGO_DEPARTAMENTO = substr(DIVIPOLA, 1, 2)) %>% 
  filter(!is.na(Area_Terreno_FIN)) 

base_consolidada %>% rm()

base_conteos_snr_avaluos <- base_consolidada_RUR %>% 
  group_by(CODIGO_DEPARTAMENTO, DIVIPOLA, FUENTE_INFO_V2) %>% count() %>%
  ungroup() %>% 
  left_join(df_coord_plane_mpio) %>% 
  left_join(df_coord_plane_dpto, by = "CODIGO_DEPARTAMENTO", suffix = c("_mpio", "_dpto")) %>% 
  mutate(Latitud = ifelse(is.na(Latitud_mpio), Latitud_dpto, Latitud_mpio),
         Longitud = ifelse(is.na(Longitud_mpio), Longitud_dpto, Longitud_mpio)) %>% 
  dplyr::select(-Longitud_mpio, -Latitud_mpio, -Longitud_dpto, -Latitud_dpto)


base_conteos_snr_avaluos %>% head()
divipola_coord <- base_conteos_snr_avaluos[, c("DIVIPOLA", "Longitud", "Latitud")] %>% distinct()


sorted_mpios_distances <- map_dfr(1:nrow(divipola_coord), 
                                  function(k, dataframe = divipola_coord, vars = c("Longitud", "Latitud"), key_var = "DIVIPOLA"){
  point_of_interest <- matrix(dataframe[k, vars])
  df_centroides_v2 <- dataframe[-k, c(vars, key_var)]
  
  # Calculate Euclidean distance
  df_centroides_v2$distance <- sqrt((df_centroides_v2[vars[1]][[1]] - point_of_interest[1][[1]])^2 + (df_centroides_v2[vars[2]][[1]] - point_of_interest[2][[1]])^2)
  
  # Find row with minimum distance
  closest_row <- df_centroides_v2 %>%
    arrange(distance)
  closest_row <- closest_row %>%
    mutate(DIVIPOLA_OR = unique(dataframe[[key_var]][k]))
  return(closest_row)
}) %>% rename("DIVIPOLA_COMP" = "DIVIPOLA") %>% 
  left_join(dplyr::select(base_conteos_snr_avaluos, -c("Longitud", "Latitud", "CODIGO_DEPARTAMENTO")),
            by = c("DIVIPOLA_COMP" = "DIVIPOLA"), 
            relationship = "many-to-many") %>% 
  rename("n_comp" = "n")

sorted_mpios_distances
divipola_coord %>% filter(DIVIPOLA %in% c("08078", "08558"))

cutoff_avaluos_oft <- 100
cutoff_snr <- 400

pass_all_qcc_functions <- function(cutoff_avaluos_oft, cutoff_snr, confidence.level){
  base_conteos_snr_avaluos_comparacion <- base_conteos_snr_avaluos %>% 
    left_join(sorted_mpios_distances[, c("DIVIPOLA_COMP", "DIVIPOLA_OR", "FUENTE_INFO_V2", "distance", "n_comp")], 
              by = c("DIVIPOLA" = "DIVIPOLA_OR", "FUENTE_INFO_V2" = "FUENTE_INFO_V2")) %>% 
    dplyr::select(-c("CODIGO_DEPARTAMENTO", "Longitud", "Latitud")) %>% 
    arrange(DIVIPOLA, FUENTE_INFO_V2, distance) %>% 
    group_by(DIVIPOLA, FUENTE_INFO_V2) %>% 
    mutate(RANK = row_number(),
           CONTEO = ifelse(RANK == 1, n_comp + n, n_comp),
           CONTEO_ACUMULADO = cumsum(CONTEO)) %>% ungroup()
  
  
  base_conteos_cutoff <- base_conteos_snr_avaluos_comparacion %>% 
    filter((FUENTE_INFO_V2 =="AVALUOS_OFERTAS" & CONTEO_ACUMULADO >= cutoff_avaluos_oft) |
             (FUENTE_INFO_V2 =="TRANSACCION" & CONTEO_ACUMULADO >= cutoff_snr)) %>% 
    group_by(DIVIPOLA, FUENTE_INFO_V2) %>% filter(row_number() == 1) %>% ungroup() %>% 
    dplyr::select(DIVIPOLA, FUENTE_INFO_V2, RANK) %>% 
    rename("CUTOFF_RANK" = "RANK")
  
  df_conteos_snr_avaluos <- base_conteos_snr_avaluos_comparacion %>% 
    left_join(base_conteos_cutoff, by = c("DIVIPOLA", "FUENTE_INFO_V2")) %>% 
    filter(RANK <= CUTOFF_RANK) %>% 
    dplyr::select(-c("RANK", "CONTEO", "CUTOFF_RANK"))
  
  lista_df_split <- df_conteos_snr_avaluos %>%
    split(paste0(.$DIVIPOLA, "_", .$FUENTE_INFO_V2))
  
  
  list_municipalities_per_group <- lista_df_split %>% 
    map_dfr(~get_list_municipalities(.x, cutoff_avaluos_oft, cutoff_snr)) %>% 
    split(paste0(.$DIVIPOLA_OR, "_", .$FUENTE_INFO_V2))
  
  
  list_dfs_estimation <- map(list_municipalities_per_group, function(.x){
    base_consolidada_RUR %>%
      inner_join(unique(.x[, c("DIVIPOLA_GROUP", "FUENTE_INFO_V2")]),
                 by = c("DIVIPOLA" = "DIVIPOLA_GROUP", "FUENTE_INFO_V2" = "FUENTE_INFO_V2"))
    
  })
  
  list_dfs_validation <- map(list_municipalities_per_group, function(.x){
    base_consolidada_RUR %>%
      inner_join(unique(.x[, c("DIVIPOLA_OR", "FUENTE_INFO_V2")]), 
                 by = c("DIVIPOLA" = "DIVIPOLA_OR", "FUENTE_INFO_V2" = "FUENTE_INFO_V2"))
  })
  
  df_final_after_qcc <- map2_dfr(list_dfs_estimation, list_dfs_validation,
                                 ~make_qcc_over_df(df_used_for_estimating = .x, 
                                                   df_to_validate = .y, 
                                                   confidence.level = confidence.level))
  
  
  df_final_after_qcc %>% saveRDS(paste0("output/df_final_after_qcc_", 
                                        cutoff_avaluos_oft ,"_", 
                                        cutoff_snr, "_",
                                        confidence.level, ".rds"))
  return(df_final_after_qcc)
}

pass_all_qcc_functions(cutoff_avaluos_oft = 100, 
                       cutoff_snr = 400, 
                       confidence.level = 0.90)

pass_all_qcc_functions(cutoff_avaluos_oft = 100, 
                       cutoff_snr = 400, 
                       confidence.level = 0.95)

pass_all_qcc_functions(cutoff_avaluos_oft = 100, 
                       cutoff_snr = 400, 
                       confidence.level = 0.99)


pass_all_qcc_functions(cutoff_avaluos_oft = 150, 
                       cutoff_snr = 500, 
                       confidence.level = 0.90)

pass_all_qcc_functions(cutoff_avaluos_oft = 150, 
                       cutoff_snr = 500, 
                       confidence.level = 0.95)

pass_all_qcc_functions(cutoff_avaluos_oft = 150, 
                       cutoff_snr = 500, 
                       confidence.level = 0.99)

pass_all_qcc_functions(cutoff_avaluos_oft = 50, 
                       cutoff_snr = 300, 
                       confidence.level = 0.90)

pass_all_qcc_functions(cutoff_avaluos_oft = 50, 
                       cutoff_snr = 300, 
                       confidence.level = 0.95)

pass_all_qcc_functions(cutoff_avaluos_oft = 50, 
                       cutoff_snr = 300, 
                       confidence.level = 0.99)

pass_all_qcc_functions(cutoff_avaluos_oft = 200, 
                       cutoff_snr = 600, 
                       confidence.level = 0.90)

pass_all_qcc_functions(cutoff_avaluos_oft = 200, 
                       cutoff_snr = 600, 
                       confidence.level = 0.95)

pass_all_qcc_functions(cutoff_avaluos_oft = 200, 
                       cutoff_snr = 600, 
                       confidence.level = 0.99)


df_final_after_qcc_v95 <- map2_dfr(list_dfs_estimation, list_dfs_validation,
                               ~make_qcc_over_df(df_used_for_estimating = .x, 
                                                 df_to_validate = .y, confidence.level = 0.95))

df_final_after_qcc_v93 <- map2_dfr(list_dfs_estimation, list_dfs_validation,
                               ~make_qcc_over_df(df_used_for_estimating = .x, 
                                                 df_to_validate = .y, confidence.level = 0.93))

df_final_after_qcc_v90 <- map2_dfr(list_dfs_estimation, list_dfs_validation,
                               ~make_qcc_over_df(df_used_for_estimating = .x, 
                                                 df_to_validate = .y, confidence.level = 0.90))




df_final_after_qcc_v90[which.max(df_final_after_qcc_v90$VALOR_TERRENO_COM_HECTAREA),] %>% 
  dplyr::select(FUENTE_INFO, DIVIPOLA, VALOR_TERRENO_COM_HECTAREA, Area_Terreno_FIN, Avaluo_Terreno)

df_final_after_qcc_v90 %>% filter(DIVIPOLA == c("70230", "70204")) %>% 
  dplyr::select(FUENTE_INFO, DIVIPOLA, VALOR_TERRENO_COM_HECTAREA, Area_Terreno_FIN, Avaluo_Terreno) %>% 
  filter(FUENTE_INFO != "SNR")

base_consolidada_RUR %>% filter(DIVIPOLA %in% c("70230", "70204")) %>% 
  dplyr::select(FUENTE_INFO, DIVIPOLA, VALOR_TERRENO_COM_HECTAREA, Area_Terreno_FIN, Avaluo_Terreno) %>% 
  filter(FUENTE_INFO != "SNR") %>% 
  arrange(desc(VALOR_TERRENO_COM_HECTAREA))

boxplot(df_final_after_qcc_v90$VALOR_TERRENO_COM_HECTAREA, plot = FALSE)

df_final_after_qcc_v90 %>% mutate(CODIGO_TERRENO = substr(CODIGO_PREDIAL, 1, 21),
                                  CODIGO_MANZANA = substr(CODIGO_PREDIAL, 1, 17)) %>% 
  group_by(CODIGO_TERRENO, FUENTE_INFO_V2) %>% 
  summarise(MEDIAN = median(VALOR_TERRENO_COM_HECTAREA)) %>% 
  pivot_wider(id_cols = "CODIGO_TERRENO", names_from = "FUENTE_INFO_V2", values_from = "MEDIAN", names_prefix = "VALOR_TERRENO_") %>% 
  filter(!is.na(VALOR_TERRENO_AVALUOS_OFERTAS) & !is.na(VALOR_TERRENO_TRANSACCION)) %>% 
  ggplot() + geom_point(aes(VALOR_TERRENO_TRANSACCION, VALOR_TERRENO_AVALUOS_OFERTAS), size = 2.5) +
  xlim(0, 200000000)


df_check <- df_final_after_qcc_v90 %>% 
  filter(FUENTE_INFO_V2 == "TRANSACCION" & VALOR_TERRENO_COM_HECTAREA > 20000000000) %>% 
  dplyr::select(VALOR_TERRENO_COM_HECTAREA, DIVIPOLA, CODIGO_PREDIAL) %>% 
  filter(DIVIPOLA == "13654")


base_consolidada_RUR %>% 
  filter(FUENTE_INFO_V2 == "TRANSACCION" & VALOR_TERRENO_COM_HECTAREA > 20000000000) %>% 
  dplyr::select(VALOR_TERRENO_COM_HECTAREA, DIVIPOLA, CODIGO_PREDIAL) %>% 
  filter(DIVIPOLA == "13654")
  arrange(desc(VALOR_TERRENO_COM_HECTAREA))

  df_final_after_qcc_v90$DESTINO_ECONOMICO %>% table()
  
df_final_after_qcc_v90 %>% 
  filter(DESTINO_ECONOMICO %in% c("D", "L", "S")) %>% 
  mutate(CODIGO_TERRENO = substr(CODIGO_PREDIAL, 1, 21),
                                  CODIGO_MANZANA = substr(CODIGO_PREDIAL, 1, 17)) %>% 
  group_by(DIVIPOLA, CODIGO_TERRENO, FUENTE_INFO_V2) %>% 
  summarise(MEDIAN = median(VALOR_TERRENO_COM_HECTAREA)) %>% 
  pivot_wider(id_cols = c("DIVIPOLA", "CODIGO_TERRENO"), names_from = "FUENTE_INFO_V2", values_from = "MEDIAN", names_prefix = "VALOR_TERRENO_") %>% 
  filter(!is.na(VALOR_TERRENO_AVALUOS_OFERTAS) & !is.na(VALOR_TERRENO_TRANSACCION)) %>% 
  ggplot() + geom_point(aes(VALOR_TERRENO_TRANSACCION, VALOR_TERRENO_AVALUOS_OFERTAS, color = DIVIPOLA), size = 2.5) +
  xlim(0, 100000000) + ylim(0, 600000000) +
  geom_smooth(aes(VALOR_TERRENO_TRANSACCION, VALOR_TERRENO_AVALUOS_OFERTAS)) +
  guides(color="none")


df_comparision_SNR <- df_final_after_qcc_v90 %>% 
  filter(DESTINO_ECONOMICO %in% c("D", "L", "S")) %>% 
  mutate(CODIGO_TERRENO = substr(CODIGO_PREDIAL, 1, 21),
         CODIGO_MANZANA = substr(CODIGO_PREDIAL, 1, 17)) %>% 
  group_by(DIVIPOLA, CODIGO_TERRENO, FUENTE_INFO_V2) %>% 
  summarise(MEDIAN = median(VALOR_TERRENO_COM_HECTAREA)) %>% ungroup() %>% 
  pivot_wider(id_cols = c("DIVIPOLA", "CODIGO_TERRENO"), names_from = "FUENTE_INFO_V2", values_from = "MEDIAN", names_prefix = "VALOR_TERRENO_") %>% 
  filter(!is.na(VALOR_TERRENO_AVALUOS_OFERTAS) & !is.na(VALOR_TERRENO_TRANSACCION)) %>% 
  mutate(LOG_VALOR_TERRENO_AVALUOS_OFERTAS = log(VALOR_TERRENO_AVALUOS_OFERTAS),
         LOG_VALOR_TERRENO_TRANSACCION = log(VALOR_TERRENO_TRANSACCION))

df_comparision_SNR_v2 <- make_qcc_over_df(df_used_for_estimating = df_comparision_SNR,
                 df_to_validate = df_comparision_SNR, confidence.level = 0.90,
                 vars = c("LOG_VALOR_TERRENO_AVALUOS_OFERTAS", "LOG_VALOR_TERRENO_TRANSACCION"))

df_comparision_SNR_v2 %>% ggplot() + 
  geom_point(aes(LOG_VALOR_TERRENO_TRANSACCION, LOG_VALOR_TERRENO_AVALUOS_OFERTAS), size = 2.5) +
  geom_smooth(aes(LOG_VALOR_TERRENO_TRANSACCION, LOG_VALOR_TERRENO_AVALUOS_OFERTAS, group = DIVIPOLA), se = FALSE, method = "lm") + 
  guides(color = FALSE)

fit <- lm(LOG_VALOR_TERRENO_AVALUOS_OFERTAS ~ VALOR_TERRENO_TRANSACCION - 1, data = df_comparision_SNR_v2)
fit
plot(fit)

# df_final_after_qcc <- map2(list_dfs_estimation, list_dfs_validation, 
#                                ~make_qcc_over_df(df_used_for_estimating = .x, df_to_validate = .y))
# 
# list_dfs_estimation %>% length
# list_dfs_validation %>% length
# 
# 
# 
# make_plot_qcc(qcc)
# 
# df_check %>% mutate(OUTLIER = case_when(1:nrow(.) %in% qcc$outlier ~ "SI",
#                                         TRUE ~ "NO")) %>%
#   ggplot() +
#   geom_point(aes(VALOR_TERRENO_COM_HECTAREA, Area_Terreno_FIN, color = OUTLIER), size = 2.5)
  



